﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository, IRepository<Preference> preferenceRepository, IRepository<Employee> employeeRepository, IRepository<Customer> customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _employeeRepository = employeeRepository;
            _customerRepository = customerRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 

                var promoCodes = await _promoCodeRepository.GetAllAsync();

                var response = promoCodes.Select(x => new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString("dd-MM-yyyy"),
                    EndDate = x.EndDate.ToString("dd-MM-yyyy"),
                    PartnerName = x.PartnerName

                }).ToList();

                return Ok(response);


        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            try
            {               
                
                var preference = await _preferenceRepository.GetFirstWhere(s => s.Name == request.Preference);
                if (preference == null) throw new Exception("Предпочтение не найдено!");
                string[] names = request.PartnerName.Split(' ');
                var firstName = names[0] ?? "";
                var lastName = names[1] ?? "";
                var partner = await _employeeRepository.GetFirstWhere(s => s.FirstName == firstName && s.LastName == lastName);
                if (partner == null) throw new Exception("Партнер не найден!");
                PromoCode promoCode = PromoCodeMapper.MapFromModel(request, preference, partner);

                await _promoCodeRepository.AddAsync(promoCode);

                var customers = await _customerRepository.GetWhere(s => s.CustomerPreferences.Select(x => x.PreferenceId).Contains(preference.Id));
                if (customers != null)
                {
                    foreach (var customer in customers)
                    {
                        customer.PromoCodes.Add(promoCode);
                        await _customerRepository.UpdateAsync(customer);
                    }
                }


            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }
    }
}