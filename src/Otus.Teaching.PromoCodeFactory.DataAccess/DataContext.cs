﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext
               : DbContext
    {
        const int MaxLength = 255;
        public DbSet<Preference> Preferences { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(cp => new { cp.CustomerId, cp.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(c => c.Customer)
                .WithMany(cp => cp.CustomerPreferences)
                .HasForeignKey(c => c.CustomerId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(p => p.Preference)
                .WithMany()
                .HasForeignKey(p => p.PreferenceId);
            
            modelBuilder.Entity<Employee>().Property(e => e.Email).HasMaxLength(MaxLength);
            modelBuilder.Entity<Employee>().Property(e => e.FirstName).HasMaxLength(MaxLength);
            modelBuilder.Entity<Employee>().Property(e => e.LastName).HasMaxLength(MaxLength);

            modelBuilder.Entity<Role>().Property(r => r.Description).HasMaxLength(MaxLength);
            modelBuilder.Entity<Role>().Property(r => r.Name).HasMaxLength(MaxLength);

            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(MaxLength);
            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(MaxLength);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(MaxLength);

            modelBuilder.Entity<Preference>().Property(p => p.Name).HasMaxLength(MaxLength);

            modelBuilder.Entity<PromoCode>().Property(p => p.Code).HasMaxLength(MaxLength);
            modelBuilder.Entity<PromoCode>().Property(p => p.PartnerName).HasMaxLength(MaxLength);
            modelBuilder.Entity<PromoCode>().Property(p => p.ServiceInfo).HasMaxLength(MaxLength);
        }

    }
  
}
