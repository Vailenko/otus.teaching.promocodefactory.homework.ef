﻿using System;
using System.Linq;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        //TODO: Добавить список предпочтений
        public List<PromoCodeShortResponse> PromoCodes { get; set; }
        public List<PreferenceResponse> Preferences { get; set; }
        public CustomerResponse(Customer customer)
        {
            Id = customer.Id;
            Email = customer.Email;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            if (customer.CustomerPreferences != null)
            {
                Preferences = customer.CustomerPreferences.Select(s => new PreferenceResponse()
                {
                    Id = s.PreferenceId,
                    Name = s.Preference.Name
                }).ToList();

            }
        }
    }
}