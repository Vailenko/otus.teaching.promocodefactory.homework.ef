﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeRequest model, Preference preference, Employee partnerManager, PromoCode promoCode = null)
        {
            if (promoCode == null)
            {
                promoCode = new PromoCode();
                promoCode.Id = Guid.NewGuid();
            }
            promoCode.Code = model.PromoCode;
            promoCode.PartnerManager = partnerManager;
            promoCode.PartnerName = model.PartnerName;
            promoCode.Preference = preference;
            promoCode.ServiceInfo = model.ServiceInfo;
                      
            return promoCode;
        }
    }
}

